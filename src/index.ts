import { Agent, createServer } from 'http';
import * as request from 'request';

import { TraceStream } from './trace-stream';

const options = {
  headers: {
    'x-agent-from': 'proxy',
    'content-type': 'application/json'
  },
  method: 'POST',
  uri: 'http://localhost:5000/graphql'
};

const agent = new Agent({ keepAlive: true });

createServer()
  .on('request', (req, res) => {
    const origin = request(options);
    const trace = new TraceStream(agent);

    req
      .pipe(origin)
      .pipe(trace)
      .pipe(res);

  })
  .listen(process.env.PORT || 4000, () => {
    console.log(`Proxy started on http://localhost:4000`);
  });
