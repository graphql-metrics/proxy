import * as request from 'request';
import { Transform } from 'stream';

export class TraceStream extends Transform {
  public buffer = [];

  constructor(private agent) {
    super();
  }

  public _transform(chunk, encoding, done) {
    this.buffer.push(chunk);
    done();
  }

  public _flush(done) {
    const buffer = Buffer.concat(this.buffer);
    const data = JSON.parse(buffer.toString());

    try {
      const json = data.extensions.tracing;
      delete data.extensions;

      request.post('http://localhost:8888/', { json });

    } catch (err) {
      if (err.name !== 'TypeError') {
        process.stderr.write(`proxy err: ${err.stack}\n`);
      }
    }

    try {
      const json = data.errors;

      request.post('http://localhost:8888/errors', { json });

    } catch (err) {
      if (err.name !== 'TypeError') {
        process.stderr.write(`proxy err: ${err.stack}\n`);
      }
    }

    done(null, JSON.stringify(data));
  }
}
