import { graphiqlExpress, graphqlExpress } from 'apollo-server-express';
import * as bodyParser from 'body-parser';
import { spawn } from 'child_process';
import * as express from 'express';
import { readFileSync } from 'fs';
import { addMockFunctionsToSchema, makeExecutableSchema } from 'graphql-tools';
import { resolve } from 'path';
import * as request from 'request';

const PORT = 3000;
const schema = makeExecutableSchema({
  typeDefs: readFileSync(__dirname + '/typedefs.graphql', 'utf8')
});
addMockFunctionsToSchema({ schema });

const app = express();
const args = [
  resolve(__dirname, '../src/index.ts'),
  '--port', '5000',
  '--secret', 'proxy',
  '--key', 'API_KEY'
];
const proxy = spawn('ts-node', args);
proxy.stdout.pipe(process.stdout);
proxy.stderr.pipe(process.stderr);
process.on('exit', () => proxy.kill());

const middleware = (req, res, next) => {
  if (req.headers['x-agent-from'] === 'proxy') {
    // console.log('next 1');
    return next();
  } else if (req.method !== 'GET' && req.method !== 'POST') {
    // console.log('next 2');
    return next();
  } else if (req.path !== '/') {
    // console.log('next 3');
    return next();
  } else {
    // console.log('proxy');
    const options = { url: 'http://localhost:4000/graphql', method: 'POST' };
    return req
      .pipe(request(options))
      .pipe(res);
  }
};

app.use(
  '/graphql',
  middleware,
  bodyParser.json(),
  graphqlExpress({
    // cacheControl: true,
    // debug: true,
    schema,
    tracing: true
  }));

app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));

app.listen(5000, () => {
  console.log(`GraphQL running on http://localhost:5000/graphql`);
});
